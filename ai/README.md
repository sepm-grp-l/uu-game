# Software Engineering and Project Management 1DL251 Fall 2021 Uppsala University - The UU-Game
Repository for Software Engineering and Project Management course at Uppsala University.

The original code belonged to group B and its members. It had been modified to work with the other modules we built and acquired.

The engine uses a minimax algorithm with alpha-beta pruning to find good moves. The heuristics functions are based on this blog: https://kartikkukreja.wordpress.com/2014/03/17/heuristicevaluation-function-for-nine-mens-morris/  
  
### How-to-use: Engine
The engine can be run from the terminal `python3 game_engine.py {server_ip} {server_port} {difficulty}`.
This will result in the engine starting up, attempting to register itself to the communication platform, then listen to events and respond accordingly.

The game engine will always make move for the player assigned by the communication platform. The player options are 'white' and 'black'.

After each move the engine has made it will increase the turn number by 1. If the engine places a piece it will decrease the amount of pieces left in the hand for the player it made the move for. If the engine manages to get three pieces in a row it will remove one piece from the opponent and decrease their pieces left count by 1.

There is a parameter called board size which isn't used by the engine but might be needed by other components wishing to use the engine.  

The last parameter, lines, describes the lines on the board as a list of lists. Each inner list describe one line on the board and what positions are in that line and which player owns that position.
One position can be part of several lines and will therefore be entered in every list describing the lines. If a position has two different owners in different lines the engine might unexectedly crash. It is therefore important that all position that are in multiple lines always have the same owner.    
      
### Testing
There are currently no tests for the heuristics. The engine and most of its functions are tested. 
To run the tests use the command:
`python3 -m unittest` in the root folder. (Not in the folder src or test)

When creating new test files please use the naming format: test_*.py where * is the thing you're testing.
