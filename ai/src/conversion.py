def owner_to_num (owner):
  if owner == 'white':
    return 1
  if owner == 'black':
    return 0
  return None

def num_to_owner (num):
  if num == 1:
    return 'white'
  if num == 0:
    return 'black'
  return 'none'

def pos_to_num (pos):
    if pos == [1,1]:
        return 0
    if pos == [1,4]:
        return 1
    if pos == [1,7]:
        return 2
    if pos == [2,2]:
        return 3
    if pos == [2,4]:
        return 4
    if pos == [2,6]:
        return 5
    if pos == [3,3]:
        return 6
    if pos == [3,4]:
        return 7
    if pos == [3,5]:
        return 8
    if pos == [4,1]:
        return 9
    if pos == [4,2]:
        return 10
    if pos == [4,3]:
        return 11
    if pos == [4,4]:
        return 12
    if pos == [4,5]:
        return 13
    if pos == [4,6]:
        return 14
    if pos == [4,7]:
        return 15
    if pos == [5,3]:
        return 16
    if pos == [5,4]:
        return 17
    if pos == [5,5]:
        return 18
    if pos == [6,2]:
        return 19
    if pos == [6,4]:
        return 20
    if pos == [6,6]:
        return 21
    if pos == [7,1]:
        return 22
    if pos == [7,4]:
        return 23
    if pos == [7,7]:
        return 24

def num_to_pos (num):
    if num == 0 :
        return [1,1]
    if num == 1 :
        return [1,4]
    if num == 2 :
        return [1,7]
    if num == 3 :
        return [2,2]
    if num == 4 :
        return [2,4]
    if num == 5 :
        return [2,6]
    if num == 6 :
        return [3,3]
    if num == 7 :
        return [3,4]
    if num == 8 :
        return [3,5]
    if num == 9 :
        return [4,1]
    if num == 10:
        return [4,2]
    if num == 11:
        return [4,3]
    if num == 12:
        return [4,4]
    if num == 13:
        return [4,5]
    if num == 14:
        return [4,6]
    if num == 15:
        return [4,7]
    if num == 16:
        return [5,3]
    if num == 17:
        return [5,4]
    if num == 18:
        return [5,5]
    if num == 19:
        return [6,2]
    if num == 20:
        return [6,4]
    if num == 21:
        return [6,6]
    if num == 22:
        return [7,1]
    if num == 23:
        return [7,4]
    if num == 24:
        return [7,7]