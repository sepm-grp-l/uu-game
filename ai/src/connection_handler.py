import socket
import os
import sys


class ConnectionHandler:
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server = sys.argv[1]
        self.port = sys.argv[2]
        self.client.connect((self.server, int(self.port)))

    def receive(self):
        return self.client.recv(1024 * 4).decode()

    def send(self, data, receive = True):
        try:
            self.client.send(str.encode(data))
            if receive: return self.receive()
        except socket.error as e:
            print(e)

    def close(self):
        try:
            self.client.shutdown(socket.SHUT_WR)
            self.client.close()
        except:
            pass

connection_handler = ConnectionHandler()
