
# UU-Game #

This repository contains the source code of an online tabletop game that allows players to connect to the same server and play a game together or with an AI.

## Demo videos
[Youtube Playlist](https://www.youtube.com/playlist?list=PLyWcXVTVpC6e7woPIsWJ3nAU3WTEKCLZV)
  

The server host is able to select one of provided sever modes:

- start a tournament (up to 8 players)

- start 1-1 game

- start a game against an AI

### Server configuration

You can modify an ip address and the port of socket, that the server will use for communication with clients, by modifying .env files in `communication-platform` and `client` folders

  

### Setup and run

To start a server you must have [Python 3.8 (or later)](https://www.python.org/downloads/) installed.

From the project directory run the following commands in terminal:

  

1. Install dependencies for communication platform

```

pip3 install -r communication-platform/requirements.txt

python3 communication-platform/server.py

```

  

2. Select game mode

3. Start clients (each command will start a client), enter below in multiple terminals to start multiple clients

```

python3 client/main.py

```

4. Play the game, (For tournament mode, the server host must press `Enter` to start the tournament)

---

The components are developed/integrated by Group L (SEPM 1DL251-H21-11007, Uppsala University)