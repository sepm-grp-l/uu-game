import graphics
import input_handler
import game_state
import commands
from commands import CommandType
from connection_handler import connection_handler
import os
from color import Color
import traceback

board_connections = list({
    1: [2, 4, 10],
    2: [1, 3, 5],
    3: [2, 6, 16],
    4: [1, 5, 11],
    5: [4, 2, 6, 8],
    6: [3, 5, 15],
    7: [12, 8],
    8: [7, 5, 9, 13],
    9: [8, 14],
    10: [1, 11, 23],
    11: [4, 10, 12, 20],
    12: [11, 7, 17, 13],
    13: [8, 12, 14, 18],
    14: [9, 13, 15, 19],
    15: [6, 14, 16, 22],
    16: [3, 15, 25],
    17: [12, 18],
    18: [13, 17, 19, 21],
    19: [14, 18],
    20: [11, 21, 23],
    21: [18, 20, 22, 24],
    22: [15, 21, 25],
    23: [10, 20, 24],
    24: [21, 23, 25],
    25: [16, 22, 24]
}.values())
board_connections = [[x - 1 for x in l] for l in board_connections]

mills = [
    # horizontal
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [10, 11, 12],
    [11, 12, 13],
    [12, 13, 14],
    [13, 14, 15],
    [14, 15, 16],
    [17, 18, 19],
    [20, 21, 22],
    [23, 24, 25],
    # vertical
    [1, 10, 23],
    [4, 11, 20],
    [7, 12, 17],
    [2, 5, 8],
    [5, 8, 13],
    [8, 13, 18],
    [13, 18, 21],
    [18, 21, 24],
    [9, 14, 19],
    [6, 15, 22],
    [3, 16, 25]
]
mills = [[x - 1 for x in l] for l in mills]

ihandler = input_handler.InputHandler()
ghandler = graphics.GraphicsHandler()

registration = None
my_name = None

p1_name = None
p1_side = None

p2_name = None
p2_side = None

def end_game():
    print('Game Ended')
    print('Final score:', connection_handler.receive())
    connection_handler.close()
    os._exit(1)

def initialize_new_game():
    global registration
    global my_name

    global p1_name
    global p1_side

    global p2_name
    global p2_side

    print('   Waiting for a game...')
    while True:
        registration = connection_handler.receive()
        if not registration:
            print('Received nothing, exiting')
            connection_handler.close()
            os._exit(1)
        elif 'game_ready' in registration:
            parts = registration.split(':')
            my_color = Color(int(parts[1]))
            
            if my_color == Color.Black:
                p1_name = my_name
                p1_side = my_color
                p2_name = parts[2]
                p2_side = Color((int(parts[1]) + 1) % 2)
            else:
                p2_name = my_name
                p2_side = my_color
                p1_name = parts[2]
                p1_side = Color((int(parts[1]) + 1) % 2)
            print(f"   Game ready you are player ({graphics.color_to_ascii(my_color)}) playing against {p2_name if p1_name == my_name else p1_name}")
            return game_loop()
        elif registration == 'tournament_ended':
            end_game()

def game_loop():
    global registration

    num_nodes = len(board_connections)
    state = game_state.GameState(p1_name, p1_side, p2_name, p2_side,
                                 (num_nodes, board_connections, mills))

    while registration != 'tournament_ended' and registration != 'match_ended':
        ghandler.display_status(
            state.player1, state.player2, state.current_turn, state.current_player)
        ghandler.display_game(
            [node.color for node in state.board.nodes])
        ghandler.display_messages()

        current_state = state.next()
        
        if current_state == CommandType.Lost:
            winner = state.get_opponent()
            ghandler.display_winner(winner)
            connection_handler.send(state.data)

            if winner.name == my_name:
                connection_handler.send('winner')
                print('   You Won!!!')
            elif '_ai' in winner.name:
                print('   You Lost!!!')
                connection_handler.send('loser')

            return initialize_new_game()

        elif current_state == CommandType.Draw:
            ghandler.display_draw()
            connection_handler.send('draw')
            return initialize_new_game()

        if state.current_player.name == my_name:
            print(f"Player {state.current_player.name} ({graphics.color_to_ascii(state.current_player.color)}): It's your turn now ")
            cmd = ihandler.get_command(current_state)

            if isinstance(cmd, commands.Quit):
                return
            elif isinstance(cmd, commands.Surrender):
                print(f"{state.current_player.name} ({graphics.color_to_ascii(state.current_player.color)}): surrendered the game!")
                return

            state.try_command(cmd, ghandler)
        else:
            print(f"Player {state.current_player.name} ({graphics.color_to_ascii(state.current_player.color)}) is thinking...")
            try:
                if (state.current_turn != 1):
                    connection_handler.send(state.data)
 
                rcv = connection_handler.receive()

                if rcv == 'opponent_disconnected':
                    print('Your opponent forfeited the match!')
                    ghandler.display_winner(state.get_opponent())
                    return initialize_new_game()
                else:
                    try:
                        state.data = rcv
                    except:
                        print('Something went wrong. Closing the game')
                        traceback.print_exc()
                        connection_handler.close()
                        os._exit(1)

            except:
                traceback.print_exc()
                continue

            state.end_turn()

    end_game()

def main():
    global registration
    global my_name

    os.system('cls' if os.name=='nt' else 'clear')

    print("   ╔═════════════════════╗\n"
          "   ║       UU-Game       ║\n"
          "   ╚═════════════════════╝\n ")
    
    print("   Please input player name (Within 15 characters):\n"
          "   ------------------------------------------------")
    while registration == None or registration == 'username_taken':
        my_name = ihandler.get_input("   Player:  ", False)[:15]
        registration = connection_handler.register(my_name)

        if registration == 'username_taken':
            print("   Name Taken. Please choose a different name.")
    
    if registration == 'registration_accepted':
        initialize_new_game()
    else:
        print('problem with registration')

    # game_start = True
    # while game_start:
    #     ghandler.display_menu()

    #     option = ihandler.get_input("   Option([ P / Q ]):  ")

    #     if option == 'Q':
    #         while game_start:
    #             sure_exit = ihandler.get_input("   Sure to quit([ Y / N ]):  ")
    #             if sure_exit == 'Y':
    #                 print("\n   >>> Quit Game\n")
    #                 game_start = False
    #             elif sure_exit == 'N':
    #                 break
    #             else:
    #                 print("   Invalid input !\n")
    #                 continue
    #     elif option == 'P':
    #         game_loop(ihandler, ghandler)
    #     else:
    #         print("  Invalid input !\n ")


if __name__ == "__main__":
    main()
