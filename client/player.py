from dataclasses import dataclass
from color import Color


@dataclass
class Player:
    name: str
    color: Color
    coins_left_to_place: int
    pieces: int = 0

    @property
    def data(self):
        return {
            'name': self.name,
            'color': self.color.value,
            'coins_left_to_place': self.coins_left_to_place,
            'pieces': self.pieces
        }

    @data.setter
    def data(self, value):
        self.name = value['name']
        self.color = Color(int(value['color']))
        self.coins_left_to_place = int(value['coins_left_to_place'])
        self.pieces = int(value['pieces'])
