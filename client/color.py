from enum import Enum


class Color(Enum):
    Empty = None
    White = 1
    Black = 0
